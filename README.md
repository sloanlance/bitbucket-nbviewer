# bbnbviewer

__bbnbviewer__ is a simple Bitbucket FileView add-on that renders ipython notebooks.


### Development
#### Docker
To run the service using the contained Dockerfile -

* docker build -t bbnbviewer
* docker run bbnbviewer


#### Flask Dev Server
To run the service using the flask development server

* export FLASK_APP='main.py'
* flask run


#### Installing dev addon
Once the dev server is running locally, you will need to expose the service to the public internet. The easiest way to do this is with [ngrok](https://ngrok.com/docs). With ngrok setup and your dev service being tunneled to the public internet, you simply go to "Manage add-ons" (https://bitbucket.org/account/user/<user/team>/addon-management) and click "Install from add-on URL" and use `<ngrok_url>/connect.json`


## Tests

### TODO
