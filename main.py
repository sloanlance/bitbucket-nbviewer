import io
import logging
import nbformat

from flask import Flask, jsonify, request, render_template
from nbconvert import HTMLExporter

app = Flask(__name__)


logger = logging.getLogger('bbnbviewer')


@app.route('/connect.json')
def descriptor():
    """Serves the connect descriptor 'file'. Served as a dynamic view out of
    laziness to not have to remember to change the 'baseUrl' prop between
    environments"""

    return jsonify({
        "key": "notebook-viewer",
        "name": "Ipython Notebook FileView",
        "description": "Bitbucket FileView addon to display rendered ipython notebooks.",
        "baseUrl": "https://{}".format(request.host),
        "authentication": {
               "type": "jwt"
           },
        "modules": {
            "fileViews": [{
                "key": "nbviewer",
                "name": {
                    "value": "Ipython Notebook"
                },
                "url": "/notebook?repo={repository.full_name}&cset={file_cset}&path={file_path}&name={file_name}",
                "file_matches": {
                    "extensions": ["ipynb"],
                },
                "conditions": [{
                        "condition": "is_binary",
                        "invert": True
                }]
            }]
        },
        "scopes": ["repository"],
        "contexts": ["account"]
    })


@app.route('/notebook')
def notebook():
    """The default view loaded into the iframe. Collect all of the passed
    arguments WRT file location and pass them to the template."""

    repo_name = request.args.get('repo')
    cset = request.args.get('cset')
    path = request.args.get('path')
    file_name = request.args.get('name')
    jwt_token = request.args.get('jwt')

    if not all([repo_name, cset, path, file_name, jwt_token]):
        return ''

    return render_template('index.tpl', repo_name=repo_name, cset=cset,
                           path=path, file_name=file_name)


@app.route('/render', methods=['POST'])
def render():
    """Called from index.tpl when file contents have been fetched from
    bitbucket. Takes the posted content and hands it off to nbformat for
    parsing"""

    try:
        file_contents = io.BytesIO(request.data)
        nb = nbformat.read(file_contents, as_version=4)
        exporter = HTMLExporter()
        exporter.template_file = 'templates/full.tpl'

        nb_data = exporter.from_notebook_node(nb)
        return nb_data[0], 200
    except Exception as e:
        logger.exception(e)
        return 'Unable to render notebook', 500


@app.route('/healthcheck')
def healthcheck():
    """Use by the hosting PaaS. If this view fails to return 200, service
    owners get paged."""

    return jsonify({
        'status': 'ok',
        'msg': 'this always returns ok',
    })
