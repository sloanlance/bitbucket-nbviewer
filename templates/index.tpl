<html>
<head>
    {% include "head.tpl" %}
</head>
<body class="ac-content">
  <div id="content">
    <div class="loading-spinner" style="display:none"></div>
  </div>
  <script src="//bitbucket.org/atlassian-connect/all.js"></script>
  <script type="text/x-mathjax-config"">
    MathJax.Hub.Config({
      tex2jax: {
          inlineMath: [['$','$'], ['\\(','\\)']],
          processEscapes: true,
          processEnvironments: true
      },
      styles: {"#MathJax_Message":{display: "none"}},
      displayAlign: 'center',
      'HTML-CSS': {
          styles: {
            '.MathJax_Display': {'margin': 0},
            linebreaks: { automatic: true }
          }
      }
    });
  </script>

  <script async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML,Safe" id=""></script>
  <script type="text/javascript">

    function loader(e){
      document.querySelector('#content').innerHTML = e.currentTarget.responseText;
      window.MathJax.Hub.Typeset();
      window.AP.resize();
    }

    // Show a spinner if it's taking some time to fetch file contents
    setTimeout(()=>{
      document.querySelector('.loading-spinner').style.display = 'block';
    }, 350)

    // Make authenticated API request through JS bridge and replace document.body
    // with the resulting HTML
    AP.require('request', function(request){
      request({
        url: '/2.0/repositories/{{repo_name}}/src/{{cset}}/{{path}}',
        success: function(responseText){
          let request = new XMLHttpRequest();
          request.addEventListener("load", loader);
          request.open('POST', '/render');
          request.setRequestHeader("Content-Type", "application/json");
          request.send(JSON.stringify(responseText));
        }
      });
    });
  </script>
</body>
</html>
