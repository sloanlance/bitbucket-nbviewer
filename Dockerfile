FROM python:alpine
EXPOSE 8080

WORKDIR /app
COPY . /app
RUN apk add gcc musl-dev --no-cache
RUN pip install -r requirements.txt
CMD ["gunicorn", "main:app", "--worker-class", "eventlet", "-w", "4", "-b", "0.0.0.0:8080"]
